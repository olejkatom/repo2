<?php
// HTTP
define('HTTP_SERVER', 'http://magazine/admin/');
define('HTTP_CATALOG', 'http://magazine/');

// HTTPS
define('HTTPS_SERVER', 'http://magazine/admin/');
define('HTTPS_CATALOG', 'http://magazine/');

// DIR
define('DIR_APPLICATION', 'Z:\home\magazine\www/admin/');
define('DIR_SYSTEM', 'Z:\home\magazine\www/system/');
define('DIR_DATABASE', 'Z:\home\magazine\www/system/database/');
define('DIR_LANGUAGE', 'Z:\home\magazine\www/admin/language/');
define('DIR_TEMPLATE', 'Z:\home\magazine\www/admin/view/template/');
define('DIR_CONFIG', 'Z:\home\magazine\www/system/config/');
define('DIR_IMAGE', 'Z:\home\magazine\www/image/');
define('DIR_CACHE', 'Z:\home\magazine\www/system/cache/');
define('DIR_DOWNLOAD', 'Z:\home\magazine\www/download/');
define('DIR_LOGS', 'Z:\home\magazine\www/system/logs/');
define('DIR_CATALOG', 'Z:\home\magazine\www/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'magazine');
define('DB_PREFIX', 'oc_');
?>